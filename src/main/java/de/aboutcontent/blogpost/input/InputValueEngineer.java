package de.aboutcontent.blogpost.input;

import java.util.Collections;
import java.util.List;

import de.espirit.firstspirit.client.access.editor.Node;
import de.espirit.firstspirit.client.access.editor.ValueEngineer;
import de.espirit.firstspirit.client.access.editor.ValueEngineerAspectType;

public class InputValueEngineer implements ValueEngineer<Input> {

	public Input copy(Input original) {
		return new Input(original.getText());
	}

	public <T> T getAspect(ValueEngineerAspectType<T> aspect) {
		return null;
	}

	public Input getEmpty() {
		return new Input();
	}

	public boolean isEmpty(Input value) {
		return value.isEmpty();
	}

	public Input read(List<Node> nodes) {
		for (final Node node : nodes) {
			final String nodeName = node.getName();
			if ("TEXT".equals(nodeName)) {
				return new Input(node.getText());
			}
		}
		return null;
	}

	public List<Node> write(Input value) {
		return Collections.singletonList(Node.create("TEXT", value.getText()));
	}
}
