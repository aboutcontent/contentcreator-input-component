package de.aboutcontent.blogpost.input;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;

import de.espirit.common.tools.Streams;
import de.espirit.firstspirit.access.store.templatestore.gom.YesNo;
import de.espirit.firstspirit.aspects.AspectMap;
import de.espirit.firstspirit.aspects.AspectType;
import de.espirit.firstspirit.module.GadgetContext;
import de.espirit.firstspirit.webedit.server.gadgets.WebPluginGadget;
import de.espirit.firstspirit.webedit.server.gadgets.aspects.SerializingValueHolder;

public class InputWebPluginGadget implements WebPluginGadget<HashMap<String, Serializable>>, SerializingValueHolder<Input, HashMap<String, Serializable>> {
	private final AspectMap aspects;
	private final GadgetContext<GomInput> context;
	private Input value = null;

	public InputWebPluginGadget(final GadgetContext<GomInput> context) {
		this.context = context;
		aspects = new AspectMap();
		aspects.put(SerializingValueHolder.TYPE, this);
	}

	public HashMap<String, Serializable> getWidgetConfiguration() {
		final HashMap<String, Serializable> configuration = new HashMap<>();
		configuration.put("label", context.getGom().label(context.getDisplayLanguage().getAbbreviation()));
		configuration.put("editable", context.getGom().editable() == YesNo.YES);
		return configuration;
	}

	public <T> T getAspect(AspectType<T> aspect) {
		return aspects.get(aspect);
	}

	public String getView() {
		return getResource("/input.html");
	}

	public HashMap<String, Serializable> getSerializedValue() {
		HashMap<String, Serializable> result = new HashMap<>();
		result.put("TEXT", value.getText());
		return result;
	}

	public Input getValue() {
		return value;
	}

	public boolean isEmpty() {
		return value == null || value.isEmpty();
	}

	public void setSerializedValue(HashMap<String, Serializable> serialization) {
		if (serialization != null && serialization.containsKey("TEXT")) {
			value = new Input((String) serialization.get("TEXT"));
		} else {
			value = new Input();
		}
	}

	public void setValue(Input value) {
		this.value = value;
	}

	private static String getResource(final String filename) {
		try {
			final InputStream stream = InputWebPluginGadget.class.getResourceAsStream(filename);
			if (stream != null) {
				return Streams.toString(stream, "UTF-8");
			}
		} catch (final IOException ignored) {
		}
		return "";
	}
}
