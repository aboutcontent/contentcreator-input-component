package de.aboutcontent.blogpost.input;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import de.espirit.firstspirit.module.GadgetContext;
import de.espirit.firstspirit.webedit.server.gadgets.WebPluginGadget;

public class InputWebPluginGadgetFactory implements de.espirit.firstspirit.webedit.server.gadgets.WebPluginGadgetFactory<GomInput, HashMap<String, Serializable>> {

	public WebPluginGadget<HashMap<String, Serializable>> create(GadgetContext<GomInput> context) {
		return new InputWebPluginGadget(context);
	}

	public String getControllerName() {
		return "InputWebGadget";
	}

	public List<String> getScriptUrls() {
		return Collections.singletonList("input/input.js");
	}

	public List<String> getStylesheetUrls() {
		return Collections.singletonList("input/input.css");
	}
}