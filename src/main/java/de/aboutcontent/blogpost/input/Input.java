package de.aboutcontent.blogpost.input;

import de.espirit.common.tools.Strings;

public class Input {

	private String text = null;

	public Input() {
	}

	public Input(String text) {
		this.text = text;
	}

	public boolean isEmpty() {
		return Strings.isEmpty(getText());
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}