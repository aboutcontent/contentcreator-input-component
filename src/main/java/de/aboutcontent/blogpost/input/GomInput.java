package de.aboutcontent.blogpost.input;

import de.espirit.common.Default;
import de.espirit.common.GomDoc;
import de.espirit.firstspirit.access.store.templatestore.gom.AbstractGomFormElement;
import de.espirit.firstspirit.access.store.templatestore.gom.YesNo;

public class GomInput extends AbstractGomFormElement {

	private static final long serialVersionUID = 7236007077460319001L;
	private YesNo editable = YesNo.YES;

	@Override
	protected String getDefaultTag() {
		return "GOM_INPUT";
	}

	@GomDoc(description = "Component has read-only-mode", since = "1.0")
	@Default("true")
	public YesNo getEditable() {
		return editable;
	}

	/**
	 * Set value of gom attribute 'editable'.
	 *
	 * @param value
	 * @see #getEditable()
	 * @see #editable()
	 */
	public void setEditable(final YesNo editable) {
		this.editable = editable;
	}

	/**
	 * Convenience method to get the value of gom attribute 'editable'. Default
	 * value is 'true'
	 * 
	 * @return true/false
	 * @see #getEditable()
	 * @see #setEditable(de.espirit.firstspirit.access.store.templatestore.gom.YesNo)
	 */
	public YesNo editable() {
		return editable;
	}

}
