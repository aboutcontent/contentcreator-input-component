package de.aboutcontent.blogpost.input;

import de.espirit.firstspirit.client.access.editor.ValueEngineer;
import de.espirit.firstspirit.client.access.editor.ValueEngineerContext;
import de.espirit.firstspirit.client.access.editor.ValueEngineerFactory;

public class InputValueEngineerFactory implements ValueEngineerFactory<Input, GomInput> {

	public Class<Input> getType() {
		return Input.class;
	}

	public ValueEngineer<Input> create(ValueEngineerContext<GomInput> context) {
		return new InputValueEngineer();
	}

}
