(function () {

	/**
	 * Constructor of the WebPluginGadget controller object.
	 *
	 * @param host WebPluginGadgetHost aktuelle instanz der komponente
	 * @param configuration konfiguration der komponenten durch de.aboutcontent.blogpost.input.InputWebPluginGadget.getWidgetConfiguration()
	 * @constructor
	 */
	function CreateInstance(host, configuration) {
		const self = this;
		
		self.host = host; // WebPluginGadgetHost
		self.configuration = configuration; // Widget configuration
		self.editable = configuration.editable;
		self.readonly = false;
		self.value = undefined;
		self.input = undefined;

		/*
		 * funktionen die von firstspirit aufgerufen werden können 
		 */
		/**
		 * Initialisierung der HTML-Komponente
		 */
		self.onLoad = function () {
			self.input = self.host.getElementById('inputWidget_text');
			self.input.addEventListener("input", function() {
				self.value["TEXT"] = self.input.value;
				self.host.onModification(); // Den ContentCreator informieren, das der Wert der Komponente sich geändert hat
			});
			if (self.value) {
				self.input.value = self.value["TEXT"];
				self.input.readOnly = !self.editable || self.readonly;
			}
		};

		/**
		 * Rückgabe des aktuellen Werts der Eingabekomponente
		 *
		 * @returns undefined | {text: (string)}
		 */
		self.getValue = function () {
			return self.value;
		};

		/**
		 * Setzen des in Firstspirit gespeicherten Werts
		 *
		 * @param value Wert der in Firstspirit für diese Komponente gespeichert ist
		 */
		self.setValue = function (value) {
			self.value = value;
			if (self.input) {
				self.input.value = value["TEXT"];
			}
		};

		/**
		 * Prüfen, ob sich ein leerer Wert in der Komponente befindet 
		 *
		 * @returns {boolean}
		 */
		self.isEmpty = function () {
			return !self.value || !self.value.text;
		};

		// --------------------------------------------------------------------
		// Aspect: Labelable
		// --------------------------------------------------------------------

		/**
		 * Rückgabe des aktuellen Labels der Komponente 
		 *
		 * @returns {string}
		 */
		self.getLabel = function () {
			return self.configuration.label;
		};

		// --------------------------------------------------------------------
		// Aspect: Editable
		// --------------------------------------------------------------------

		/**
		 * Eingaben zulassen oder unterbinden
		 *
		 * @param editable neuer bearbeitungsstatus
		 */
		self.setEditable = function (editable) {
			self.readonly = !editable;
			if (self.input) {
				self.input.readonly = !self.editable || self.readonly;
			}
		};
	}

	// registrieren der methode zum erzeugen einer neuen instanz der komponente
	// referenziert durch de.aboutcontent.blogpost.input.InputWebPluginGadgetFactory.getControllerName()
	window.InputWebGadget = CreateInstance;
})();